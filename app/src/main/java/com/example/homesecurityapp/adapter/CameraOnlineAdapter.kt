package com.example.homesecurityapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.homesecurityapp.R
import com.example.homesecurityapp.model.CameraOnline
import kotlinx.android.synthetic.main.item_camera_online.view.*


class CameraOnlineAdapter(private val context: Context) : RecyclerView.Adapter<CameraOnlineAdapter.CameraOnlineViewHolder>() {

    private var cameraOnlineList = arrayListOf<CameraOnline>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CameraOnlineViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_camera_online, parent, false)
        return CameraOnlineViewHolder(view)
    }

    fun updateList(list: ArrayList<CameraOnline>) {
        cameraOnlineList.clear()
        cameraOnlineList.addAll(list)
        list.sortByDescending { it.start }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return cameraOnlineList.size
    }

    override fun onBindViewHolder(holder: CameraOnlineViewHolder, position: Int) {
        val cameraOnline = cameraOnlineList[position]
        holder.apply {
            tvCameraOnlineStart.text = context.resources.getString(R.string.co_start, cameraOnline.start)
            if (cameraOnline.end.isBlank()) {
                tvCameraOnlineEnd.text = context.resources.getString(R.string.co_end, context.resources.getString(R.string.co_end_not_found))
            } else {
                tvCameraOnlineEnd.text = context.resources.getString(R.string.co_end, cameraOnline.end)
            }
            tvCameraOnlineNumber.text = context.resources.getString(R.string.co_number_faces, cameraOnline.facesDetected.toString())
        }
    }

    class CameraOnlineViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvCameraOnlineStart: TextView = itemView.tvCoStart

        val tvCameraOnlineEnd: TextView = itemView.tvCoEnd

        val tvCameraOnlineNumber: TextView = itemView.tvCoNumber
    }
}