package com.example.homesecurityapp.adapter

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.homesecurityapp.R
import com.example.homesecurityapp.model.CapturedImage
import com.example.homesecurityapp.utils.GeneralUtils.Companion.getScreenWidth
import com.example.homesecurityapp.utils.GeneralUtils.Companion.stringToBitmap
import kotlinx.android.synthetic.main.item_face_history.view.*

class HistoryAdapter(private val context: Context) : RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {

    private var historyList = arrayListOf<CapturedImage>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_face_history, parent, false)
        return HistoryViewHolder(view)
    }

    fun updateList(list: ArrayList<CapturedImage>) {
        historyList.clear()
        list.sortByDescending { it.start }
        historyList.addAll(list)
        notifyDataSetChanged()
    }

    fun isListNew(list: ArrayList<CapturedImage>): Boolean {
        return historyList != list
    }

    override fun getItemCount(): Int {
        return historyList.size
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        val historyItem = historyList[position]
        val image = stringToBitmap(historyItem.image)
        holder.apply {
            if (image != null) {
                ivHistory.setImageBitmap(Bitmap.createScaledBitmap(image, getScreenWidth(), (getScreenWidth() / 1.7).toInt(), false))
                tvHistoryDuration.text = context.resources.getString(R.string.duration, historyItem.duration)
                tvHistoryFaceNumber.text = context.resources.getString(R.string.faces_detected, historyItem.facesDetected.toString())
                tvHistoryDate.text = context.resources.getString(R.string.capture_date, historyItem.start)
            }
        }
    }

    class HistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val ivHistory: ImageView = itemView.ivHistoryItem

        val tvHistoryDuration: TextView = itemView.tvDurationItem

        val tvHistoryFaceNumber: TextView = itemView.tvFaceNumberItem

        val tvHistoryDate: TextView = itemView.tvDateItem
    }
}