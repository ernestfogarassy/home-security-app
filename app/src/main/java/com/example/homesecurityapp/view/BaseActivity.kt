package com.example.homesecurityapp.view

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.homesecurityapp.Constants.Companion.CAMERA_PERMISSION_REQUEST_CODE
import com.example.homesecurityapp.R
import com.example.homesecurityapp.dialog.ConfirmationDialog
import com.example.homesecurityapp.handler.AuthFragmentHandler
import com.example.homesecurityapp.handler.AuthFragments
import com.example.homesecurityapp.handler.MainFragmentHandler
import com.example.homesecurityapp.handler.MainFragments
import com.example.homesecurityapp.utils.ConnectivityCallback
import com.example.homesecurityapp.utils.changeViewVisibility
import com.example.homesecurityapp.utils.toast
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_base.*


class BaseActivity : AppCompatActivity(), View.OnClickListener, ConfirmationDialog.OnConfirmationClicked, ConnectivityCallback.ConnectionChangedListener {

    private lateinit var auth: FirebaseAuth

    private var appStage: AppStage = AppStage.AUTHENTICATION

    private lateinit var confirmationDialog: ConfirmationDialog

    private lateinit var authFragmentHandler: AuthFragmentHandler

    private lateinit var mainFragmentHandler: MainFragmentHandler

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        confirmationDialog = ConfirmationDialog(this)

        ivMainFragment.setOnClickListener(this)
        ivCameraFragment.setOnClickListener(this)
        ivHistoryFragment.setOnClickListener(this)

        authFragmentHandler = AuthFragmentHandler(this)
        mainFragmentHandler = MainFragmentHandler(this)

        FirebaseApp.initializeApp(this)
        auth = Firebase.auth

        checkNetwork()
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        cm.registerDefaultNetworkCallback(ConnectivityCallback(this))
    }

    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        if (currentUser == null) {
            navigateToAuthFragment(authFragmentHandler.getShownFragment())
        } else {
            navigateToMainFragment(mainFragmentHandler.getShownFragment())
        }
    }

    override fun onBackPressed() {
        when (appStage) {
            AppStage.AUTHENTICATION -> authFragmentHandler.onBackPressed()
            AppStage.MAIN -> mainFragmentHandler.onBackPressed()
        }
    }

    fun exitApp() {
        super.onBackPressed()
    }

    fun navigateToAuthFragment(fragment: AuthFragments) {
        appStage = AppStage.AUTHENTICATION
        clNavigationBar.visibility = View.GONE
        authFragmentHandler.showFragment(fragment)
    }

    fun navigateToMainFragment(fragment: MainFragments) {
        appStage = AppStage.MAIN
        clNavigationBar.visibility = View.VISIBLE
        mainFragmentHandler.showFragment(fragment)
    }

    fun clearAuthFields() {
        authFragmentHandler.clearAuthFields()
    }

    private fun checkNetwork() {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        changeViewVisibility(!isConnected, tvNetworkWarning)
    }

    fun checkCameraPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            confirmationDialog.show(getString(R.string.confirm_camera), ConfirmationDialog.ConfirmationType.PERMISSION_CAMERA, this)
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA),
                CAMERA_PERMISSION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            CAMERA_PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                toast("Permission Granted", this)
                navigateToMainFragment(MainFragments.CAMERA)
            } else {
                toast("Permission Denied", this)
            }
        }
    }

    override fun onConnectionChanged(hasNetwork: Boolean) {
        runOnUiThread {
            changeViewVisibility(!hasNetwork, tvNetworkWarning)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivMainFragment -> {
                navigateToMainFragment(MainFragments.MENU)
            }
            R.id.ivCameraFragment -> {
                navigateToMainFragment(MainFragments.CAMERA)
            }
            R.id.ivHistoryFragment -> {
                navigateToMainFragment(MainFragments.HISTORY)
            }
        }
    }

    override fun onOkClicked(type: ConfirmationDialog.ConfirmationType) {
        when (type) {
            ConfirmationDialog.ConfirmationType.PERMISSION_CAMERA -> {
                requestPermission()
            }
        }
    }

    enum class AppStage {
        AUTHENTICATION,
        MAIN

    }
}
