package com.example.homesecurityapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.homesecurityapp.Constants.Companion.MIN_PASSWORD_LENGTH
import com.example.homesecurityapp.R
import com.example.homesecurityapp.handler.MainFragments
import com.example.homesecurityapp.handler.PreferenceHandler.Companion.deleteAllPreferences
import com.example.homesecurityapp.utils.toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment(private val baseActivity: BaseActivity) : Fragment(), View.OnClickListener {

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auth = Firebase.auth
        tvOkLogin.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvOkLogin -> {
                if (validateData()) {
                    loginUser()
                }
            }
        }
    }

    fun clearFields() {
        etEmailLogin?.text?.clear()
        etPasswordLogin?.text?.clear()
    }

    private fun validateData(): Boolean {
        var valid = true
        if (etEmailLogin.text.isBlank() || !android.util.Patterns.EMAIL_ADDRESS.matcher(etEmailLogin.text).matches()) {
            etEmailLogin.error = resources.getString(R.string.invalid_email)
            valid = false
        }
        if (etPasswordLogin.text.isBlank() || etPasswordLogin.text.length < MIN_PASSWORD_LENGTH) {
            etPasswordLogin.error = resources.getString(R.string.short_string, MIN_PASSWORD_LENGTH.toString())
            valid = false
        }
        return valid
    }

    private fun loginUser() {
        auth.signInWithEmailAndPassword(etEmailLogin.text.toString(), etPasswordLogin.text.toString())
                .addOnCompleteListener(baseActivity) { task ->
                    if (task.isSuccessful && auth.currentUser != null) {
                        baseActivity.apply {
                            deleteAllPreferences(baseActivity)
                            clearAuthFields()
                            navigateToMainFragment(MainFragments.MENU)
                        }
                    } else {
                        loginFailure(task.exception?.localizedMessage)
                    }
                }
    }

    private fun loginFailure(message: String? = null) {
        if (message == null) {
            toast(resources.getString(R.string.authentication_failed), baseActivity)
        } else {
            toast(message, baseActivity)
        }
    }
}