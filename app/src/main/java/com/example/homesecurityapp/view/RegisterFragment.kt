package com.example.homesecurityapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.homesecurityapp.Constants.Companion.KEY_VERIFICATION_CODE
import com.example.homesecurityapp.Constants.Companion.MIN_PASSWORD_LENGTH
import com.example.homesecurityapp.Constants.Companion.MIN_USERNAME_LENGTH
import com.example.homesecurityapp.R
import com.example.homesecurityapp.handler.AuthFragments
import com.example.homesecurityapp.handler.PreferenceHandler.Companion.containsPreference
import com.example.homesecurityapp.handler.RegisterFragmentHandler
import kotlinx.android.synthetic.main.fragment_register.*


class RegisterFragment(private val baseActivity: BaseActivity) : Fragment(), View.OnClickListener {

    private lateinit var registerFragmentHandler: RegisterFragmentHandler

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerFragmentHandler = RegisterFragmentHandler(baseActivity)
        if (containsPreference(KEY_VERIFICATION_CODE, baseActivity)) {
            tvSkipToCodeRegister.visibility = View.VISIBLE
        } else {
            tvSkipToCodeRegister.visibility = View.GONE
        }
        tvOkRegister.setOnClickListener(this)
        tvSkipToCodeRegister.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvOkRegister -> {
                if (validateData()) {
                    registerFragmentHandler.apply {
                        saveUserData(etUserRegister.text.toString(), etEmailRegister.text.toString(),
                                etPasswordRegister.text.toString())
                        sendEmail()
                    }
                }
            }
            R.id.tvSkipToCodeRegister -> {
                baseActivity.navigateToAuthFragment(AuthFragments.VERIFICATION)
            }
        }
    }

    fun clearFields() {
        etUserRegister?.text?.clear()
        etEmailRegister?.text?.clear()
        etPasswordRegister?.text?.clear()
    }

    private fun validateData(): Boolean {
        var valid = true
        if (etUserRegister.text.isBlank() || etUserRegister.text.length < MIN_USERNAME_LENGTH) {
            etUserRegister.error = resources.getString(R.string.short_string, MIN_USERNAME_LENGTH.toString())
            valid = false
        }
        if (etEmailRegister.text.isBlank() || !android.util.Patterns.EMAIL_ADDRESS.matcher(etEmailRegister.text).matches()) {
            etEmailRegister.error = resources.getString(R.string.invalid_email)
            valid = false
        }
        if (etPasswordRegister.text.isBlank() || etPasswordRegister.text.length < MIN_PASSWORD_LENGTH) {
            etPasswordRegister.error = resources.getString(R.string.short_string, MIN_PASSWORD_LENGTH.toString())
            valid = false
        }
        return valid
    }
}