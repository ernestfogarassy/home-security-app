package com.example.homesecurityapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homesecurityapp.Constants.Companion.KEY_SECURITY_CODE
import com.example.homesecurityapp.dialog.AgreementDialog
import com.example.homesecurityapp.R
import com.example.homesecurityapp.adapter.CameraOnlineAdapter
import com.example.homesecurityapp.dialog.SecurityDialog
import com.example.homesecurityapp.handler.AuthFragments
import com.example.homesecurityapp.handler.MainFragments
import com.example.homesecurityapp.handler.PreferenceHandler.Companion.containsPreference
import com.example.homesecurityapp.model.CameraOnline
import com.example.homesecurityapp.utils.changeViewVisibility
import com.example.homesecurityapp.utils.toast
import com.example.homesecurityapp.viewmodel.MenuFragmentViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_menu.*


class MenuFragment(private val baseActivity: BaseActivity) : Fragment(), View.OnClickListener,
        AgreementDialog.OnAgreementClicked, SecurityDialog.SecurityCodeChanged {

    private var auth = Firebase.auth

    private var cameraOnlineAdapter: CameraOnlineAdapter = CameraOnlineAdapter(baseActivity)

    private lateinit var username: String

    private val menuFragmentViewModel = MenuFragmentViewModel()

    private val agreementDialog = AgreementDialog(baseActivity)

    private val securityDialog = SecurityDialog(baseActivity, this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        llMenuCamera.setOnClickListener(this)
        llMenuDoor.setOnClickListener(this)
        tvLogoutMenu.setOnClickListener(this)
        tvUsernameMenu.setOnClickListener(this)

        rvCoActivities.layoutManager = LinearLayoutManager(requireContext())
        rvCoActivities.adapter = cameraOnlineAdapter

        val usernameObserver = Observer<String> {
            username = it
            tvUsernameMenu?.apply {
                text = it
                visibility = View.VISIBLE
            }
        }
        val cameraOnlineObserver = Observer<ArrayList<CameraOnline>> {
            if (it.isEmpty()) {
                changeViewVisibility(true, ivCoEmptyList, tvCoEmptyList)
            } else {
                changeViewVisibility(false, ivCoEmptyList, tvCoEmptyList)
            }
            cameraOnlineAdapter.updateList(it)
        }

        menuFragmentViewModel.apply {
            usernameLiveData.observe(requireActivity(), usernameObserver)
            if (!cameraOnlineMutableList.hasActiveObservers()) {
                cameraOnlineMutableList.observe(requireActivity(), cameraOnlineObserver)
            }
            queryUsername()
            queryCameraOnline()
        }

        onSecurityCodeChanged()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.llMenuCamera -> {
                baseActivity.navigateToMainFragment(MainFragments.CAMERA)
            }
            R.id.llMenuDoor -> {
                securityDialog.show()
            }
            R.id.tvLogoutMenu -> {
                agreementDialog.show(resources.getString(R.string.agree_logout),
                        AgreementDialog.AgreementType.LOGOUT, this)
            }
            R.id.tvUsernameMenu -> {
                toast(resources.getString(R.string.welcome_user, username), baseActivity)
            }
        }
    }

    override fun onYesClicked(type: AgreementDialog.AgreementType) {
        when (type) {
            AgreementDialog.AgreementType.LOGOUT -> {
                auth.signOut()
                baseActivity.navigateToAuthFragment(AuthFragments.GREETING)
            }
        }
    }

    override fun onSecurityCodeChanged() {
        if(containsPreference(KEY_SECURITY_CODE, baseActivity)){
            tvMenuDoor.text = resources.getString(R.string.change_security_code)
        } else {
            tvMenuDoor.text = resources.getString(R.string.set_security_code)
        }
    }
}