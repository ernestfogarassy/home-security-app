package com.example.homesecurityapp.view

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.homesecurityapp.Constants
import com.example.homesecurityapp.Constants.Companion.KEY_SECURITY_CODE
import com.example.homesecurityapp.R
import com.example.homesecurityapp.dialog.SecurityPromptDialog
import com.example.homesecurityapp.handler.DevicePosition
import com.example.homesecurityapp.handler.OrientationHandler
import com.example.homesecurityapp.handler.PreferenceHandler.Companion.containsPreference
import com.example.homesecurityapp.utils.GeneralUtils.Companion.saveImage
import com.example.homesecurityapp.utils.SendMailTask
import com.example.homesecurityapp.utils.changeViewVisibility
import com.example.homesecurityapp.utils.toast
import com.example.homesecurityapp.viewmodel.CameraFragmentViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_camera.*
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.objdetect.CascadeClassifier
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class CameraFragment(private val baseActivity: BaseActivity, private val codeInputCorrectly: CodeInputCorrectly) : Fragment(), OrientationHandler.OrientationChangedListener,
        View.OnClickListener, CameraFragmentViewModel.SessionCounterChange, CameraFragmentViewModel.NewFaceDetected,
        SendMailTask.MailSentListener, SecurityPromptDialog.SecurityCodeChecked {

    private lateinit var cascadeFile: File

    private var auth = Firebase.auth

    var cameraFragmentViewModel: CameraFragmentViewModel = CameraFragmentViewModel(this, this)

    private lateinit var orientationHandler: OrientationHandler

    private val securityPromptDialog = SecurityPromptDialog(baseActivity, this)

    interface CodeInputCorrectly{

        fun onCodeInput()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_camera, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, context, baseCallback)
        } else {
            baseCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
        javaCamView.apply {
//            enableFpsMeter()
            setCvCameraViewListener(cameraFragmentViewModel)
            keepScreenOn = true
        }
        orientationHandler = OrientationHandler(requireContext(), this)
        ivStartCapture.setOnClickListener(this)
        tvStartCapture.setOnClickListener(this)
        tvStopCapture.setOnClickListener(this)
    }

    private fun tryStoppingCapture(){
        if(!containsPreference(KEY_SECURITY_CODE, baseActivity)){
            onCodeChecked()
        } else if (!securityPromptDialog.isShowing) {
            securityPromptDialog.show()
        }
    }

    fun stopCapture(){
        if (cameraFragmentViewModel.isCaptureRunning) {
            tryStoppingCapture()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivStartCapture, R.id.tvStartCapture -> {
                changeViewVisibility(false, ivStartCapture, tvStartCapture)
                changeViewVisibility(true, tvStopCapture, tvLiveCamera, flFaceCount)
                tvFaceCount.text = resources.getString(R.string.faces_detected, "0")
                cameraFragmentViewModel.startCapture()
            }
            R.id.tvStopCapture -> {
                tryStoppingCapture()
            }
        }
    }

    private fun sendEmail(bitmapString: Bitmap, time: String) {
        saveImage(requireContext(), bitmapString)
        baseActivity.runOnUiThread {
            val sendMailTask = SendMailTask(baseActivity, false, true)
            sendMailTask.setMailSentListener(this)
            sendMailTask.execute(Constants.SENDER_EMAIL, Constants.SENDER_PASS,
                    auth.currentUser?.email,
                    "HomeSecurity Alert: a face has been detected",
                    "A face has been detected at: $time<BR>Captured image attached<BR>")
        }
    }

    override fun onCounterChanged(counter: Int) {
        baseActivity.runOnUiThread {
            tvFaceCount.text = resources.getString(R.string.faces_detected, counter.toString())
        }
    }

    private val baseCallback: BaseLoaderCallback = object : BaseLoaderCallback(context) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    val inputStream = resources.openRawResource(R.raw.haarcascade_frontalface_alt2)
                    val cascadeDir = activity!!.getDir("cascade", Context.MODE_PRIVATE)
                    cascadeFile = File(cascadeDir, "haarcascade_frontalface_alt2.xml")
                    val buffer = ByteArray(4096)
                    var bytesRead: Int
                    try {
                        val fos = FileOutputStream(cascadeFile)
                        while (inputStream.read(buffer).also { bytesRead = it } != -1) {
                            fos.write(buffer, 0, bytesRead)
                        }
                        inputStream.close()
                        fos.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    cameraFragmentViewModel.faceDetector = CascadeClassifier(cascadeFile.absolutePath)
                    if (cameraFragmentViewModel.faceDetector!!.empty()) {
                        cameraFragmentViewModel.faceDetector = null
                    } else {
                        cascadeDir.delete()
                    }
                    javaCamView?.enableView()
                }
                else -> super.onManagerConnected(status)
            }
        }
    }

    override fun onOrientationChanged(position: DevicePosition) {
        if (cameraFragmentViewModel.isCaptureRunning && position != DevicePosition.CORRECT_POSITION) {
            tryStoppingCapture()
        }
        if(cameraFragmentViewModel.isCaptureRunning) return
        if (!securityPromptDialog.isShowing) {
            when (position) {
                DevicePosition.CORRECT_POSITION -> {
                    changeViewVisibility(false, ivPositionArrow, tvTurnDevice1, tvTurnDevice2)
                    changeViewVisibility(true, ivStartCapture, tvStartCapture)
                }
                DevicePosition.TO_ROTATE -> {
                    changeViewVisibility(false, tvTurnDevice2, ivStartCapture, tvStartCapture)
                    changeViewVisibility(true, ivPositionArrow, tvTurnDevice1)
                }
                DevicePosition.TO_FLIP -> {
                    changeViewVisibility(false, tvTurnDevice1, ivStartCapture, tvStartCapture)
                    changeViewVisibility(true, ivPositionArrow, tvTurnDevice2)
                }
            }
        } else {
            orientationHandler.currentOrientation = null
        }
    }

    override fun onDestroy() {
        orientationHandler.disable()
        super.onDestroy()
    }

    override fun onMailSentSuccess() {
        baseActivity.runOnUiThread {
            toast("email sent", baseActivity)
        }
    }

    override fun onMailSentFailure(error: String?) {
        baseActivity.runOnUiThread {
            toast("email fail: $error", baseActivity)
        }
    }

    override fun onFaceDetected(bitmapString: Bitmap?, time: String) {
        if (bitmapString != null) {
            sendEmail(bitmapString, time)
        }
    }

    override fun onCodeChecked() {
        cameraFragmentViewModel.stopCapture()
        changeViewVisibility(false, tvStopCapture, tvLiveCamera, flFaceCount, ivStartCapture, tvStartCapture)
        orientationHandler.currentOrientation = null
        codeInputCorrectly.onCodeInput()
    }
}