package com.example.homesecurityapp.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.homesecurityapp.Constants.Companion.CODE_VALIDITY
import com.example.homesecurityapp.Constants.Companion.KEY_CODE_TIME
import com.example.homesecurityapp.Constants.Companion.KEY_EMAIL
import com.example.homesecurityapp.Constants.Companion.KEY_PASS
import com.example.homesecurityapp.Constants.Companion.KEY_TEMP_USER
import com.example.homesecurityapp.Constants.Companion.KEY_VERIFICATION_CODE
import com.example.homesecurityapp.Constants.Companion.MIN_VALIDATION_LENGTH
import com.example.homesecurityapp.R
import com.example.homesecurityapp.model.User
import com.example.homesecurityapp.handler.AuthFragments
import com.example.homesecurityapp.handler.MainFragments
import com.example.homesecurityapp.handler.PreferenceHandler.Companion.containsPreference
import com.example.homesecurityapp.handler.PreferenceHandler.Companion.deleteAllPreferences
import com.example.homesecurityapp.handler.PreferenceHandler.Companion.getStringPreference
import com.example.homesecurityapp.utils.toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_verification.*
import java.util.*


class VerificationFragment(private val baseActivity: BaseActivity) : Fragment(), View.OnClickListener {

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_verification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvOkVerification.setOnClickListener(this)
        auth = Firebase.auth
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvOkVerification -> {
                verifyCode()
            }
        }
    }

    fun clearFields() {
        etVerificationCode?.text?.clear()
    }

    @SuppressLint("SimpleDateFormat")
    private fun verifyCode() {
        if (etVerificationCode.text.isBlank() || etVerificationCode.text.length < MIN_VALIDATION_LENGTH) {
            etVerificationCode.error = resources.getString(R.string.short_string, MIN_VALIDATION_LENGTH.toString())
        } else if (!containsPreference(KEY_VERIFICATION_CODE, baseActivity) ||
                etVerificationCode.text.toString() != getStringPreference(KEY_VERIFICATION_CODE, baseActivity)) {
            etVerificationCode.error = resources.getString(R.string.incorrect_code)
        } else {
            val codeTime = getStringPreference(KEY_CODE_TIME, baseActivity)
                    ?: error("should not be null!")
            val calendar = Calendar.getInstance()
            if (calendar.timeInMillis / 60000 - codeTime.toLong() > CODE_VALIDITY) {
                toast(resources.getString(R.string.code_expired), baseActivity)
                baseActivity.apply {
                    deleteAllPreferences(this)
                    clearAuthFields()
                    navigateToAuthFragment(AuthFragments.REGISTER)
                }
            } else {
                registerUser()
            }
        }
    }

    private fun registerUser() {
        auth.createUserWithEmailAndPassword(getStringPreference(KEY_EMAIL, baseActivity)!!, getStringPreference(KEY_PASS, baseActivity)!!)
                .addOnCompleteListener(baseActivity) { task ->
                    if (task.isSuccessful) {
                        createNewUser(task.result?.user)
                    } else {
                        authenticationFailure(task.exception?.localizedMessage)
                    }
                }
    }

    private fun createNewUser(newUser: FirebaseUser?) {
        if (newUser == null) authenticationFailure()
        else {
            val email: String? = newUser.email
            val userId: String = newUser.uid

            val database = FirebaseDatabase.getInstance()
            val myRef = database.reference

            if (containsPreference(KEY_TEMP_USER, baseActivity)) {
                val user = User(getStringPreference(KEY_TEMP_USER, baseActivity)!!, email)
                myRef.child("users").child(userId).setValue(user)
                baseActivity.apply {
                    deleteAllPreferences(this)
                    clearAuthFields()
                    navigateToMainFragment(MainFragments.MENU)
                }
            }
        }

    }

    private fun authenticationFailure(message: String? = null) {
        if (message == null) {
            toast(resources.getString(R.string.authentication_failed), baseActivity)
        } else {
            toast(message, baseActivity)
        }
        baseActivity.apply {
            deleteAllPreferences(this)
            clearAuthFields()
            navigateToAuthFragment(AuthFragments.REGISTER)
        }
    }
}