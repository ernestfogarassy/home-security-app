package com.example.homesecurityapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homesecurityapp.R
import com.example.homesecurityapp.adapter.HistoryAdapter
import com.example.homesecurityapp.model.CapturedImage
import com.example.homesecurityapp.utils.changeViewVisibility
import com.example.homesecurityapp.viewmodel.HistoryFragmentViewModel
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment(baseActivity: BaseActivity) : Fragment() {

    private var historyAdapter: HistoryAdapter = HistoryAdapter(baseActivity)

    private val historyFragmentViewModel = HistoryFragmentViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvHistory.layoutManager = LinearLayoutManager(requireContext())
        rvHistory.adapter = historyAdapter

        val historyObserver = Observer<ArrayList<CapturedImage>> {
            if (it.isEmpty()) {
                changeViewVisibility(true, ivHistoryEmpty, tvHistoryEmpty)
            } else {
                changeViewVisibility(false, ivHistoryEmpty, tvHistoryEmpty)
            }
            if (historyAdapter.isListNew(it)) {
                historyAdapter.updateList(it)
                rvHistory?.scrollToPosition(0)
            }
        }
        historyFragmentViewModel.historyListLiveData.observe(requireActivity(), historyObserver)
        historyFragmentViewModel.queryCapturedImage()
    }
}