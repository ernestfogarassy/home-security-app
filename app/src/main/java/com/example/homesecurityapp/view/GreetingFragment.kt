package com.example.homesecurityapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.homesecurityapp.Constants
import com.example.homesecurityapp.R
import com.example.homesecurityapp.handler.AuthFragments
import com.example.homesecurityapp.handler.PreferenceHandler
import kotlinx.android.synthetic.main.fragment_greeting.*

class GreetingFragment(private val baseActivity: BaseActivity) : Fragment(), View.OnClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_greeting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(PreferenceHandler.containsPreference(Constants.KEY_VERIFICATION_CODE, baseActivity)){
            tvSkipToCodeGreeting.visibility = View.VISIBLE
        } else {
            tvSkipToCodeGreeting.visibility = View.GONE
        }
        tvGreetingLogin.setOnClickListener(this)
        tvGreetingRegister.setOnClickListener(this)
        tvSkipToCodeGreeting.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvGreetingLogin -> {
                baseActivity.navigateToAuthFragment(AuthFragments.LOGIN)
            }
            R.id.tvGreetingRegister -> {
                baseActivity.navigateToAuthFragment(AuthFragments.REGISTER)
            }
            R.id.tvSkipToCodeGreeting -> {
                baseActivity.navigateToAuthFragment(AuthFragments.VERIFICATION)
            }
        }
    }
}