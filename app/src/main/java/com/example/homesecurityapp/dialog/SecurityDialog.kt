package com.example.homesecurityapp.dialog

import android.app.ActionBar
import android.app.Activity
import android.app.Dialog
import android.view.View
import com.example.homesecurityapp.Constants.Companion.KEY_SECURITY_CODE
import com.example.homesecurityapp.Constants.Companion.MIN_SECURITY_CODE_LENGTH
import com.example.homesecurityapp.R
import com.example.homesecurityapp.handler.PreferenceHandler.Companion.containsPreference
import com.example.homesecurityapp.handler.PreferenceHandler.Companion.getStringPreference
import com.example.homesecurityapp.handler.PreferenceHandler.Companion.saveStringPreference
import com.example.homesecurityapp.utils.changeViewVisibility
import kotlinx.android.synthetic.main.dialog_security.*

class SecurityDialog(private val activity: Activity, private val securityCodeChanged: SecurityCodeChanged) : Dialog(activity), View.OnClickListener {

    interface SecurityCodeChanged {

        fun onSecurityCodeChanged()
    }

    init {
        val width = (context.resources.displayMetrics.widthPixels * 0.90).toInt()
        setContentView(R.layout.dialog_security)
        window?.setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT)

        ivSetSecurityCode.setOnClickListener(this)
        ivCancelSecurityCode.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivSetSecurityCode -> {
                if (checkFields()) {
                    saveStringPreference(KEY_SECURITY_CODE, etSetSecurityCode.text.toString(), activity)
                    securityCodeChanged.onSecurityCodeChanged()
                    clearFields()
                    dismiss()
                }
            }
            R.id.ivCancelSecurityCode -> {
                clearFields()
                dismiss()
            }
        }
    }

    override fun show() {
        changeViewVisibility(containsPreference(KEY_SECURITY_CODE, activity), etOldSecurityCode)
        super.show()
    }

    private fun clearFields() {
        etOldSecurityCode.text.clear()
        etSetSecurityCode.text.clear()
        etSetSecurityCode2.text.clear()
    }

    private fun checkFields(): Boolean {
        when {
            containsPreference(KEY_SECURITY_CODE, activity) &&
                    getStringPreference(KEY_SECURITY_CODE, activity) != etOldSecurityCode.text.toString() -> {
                etOldSecurityCode.error = activity.resources.getString(R.string.bad_old_code)
                return false
            }
            etSetSecurityCode.text.length < MIN_SECURITY_CODE_LENGTH -> {
                etSetSecurityCode.error = activity.resources.getString(R.string.short_string,
                        MIN_SECURITY_CODE_LENGTH.toString())
                return false
            }
            etSetSecurityCode.text.toString() != etSetSecurityCode2.text.toString() -> {
                etSetSecurityCode2.error = activity.resources.getString(R.string.codes_must_match)
                return false
            }
            else -> return true
        }
    }
}