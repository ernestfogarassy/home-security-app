package com.example.homesecurityapp.dialog

import android.app.ActionBar
import android.app.Activity
import android.app.Dialog
import android.view.View
import com.example.homesecurityapp.Constants.Companion.KEY_SECURITY_CODE
import com.example.homesecurityapp.R
import com.example.homesecurityapp.handler.PreferenceHandler.Companion.containsPreference
import com.example.homesecurityapp.handler.PreferenceHandler.Companion.getStringPreference
import com.example.homesecurityapp.utils.AlarmPlayer
import kotlinx.android.synthetic.main.dialog_security_prompt.*


class SecurityPromptDialog(private val activity: Activity, private val securityCodeChecked: SecurityCodeChecked) : Dialog(activity), View.OnClickListener {

    private var botchedAttempts: Int = 0

    private val alarmPlayer = AlarmPlayer(activity)

    interface SecurityCodeChecked {

        fun onCodeChecked()
    }

    init {
        if (!containsPreference(KEY_SECURITY_CODE, activity)) {
            dismiss()
        }
        val width = (context.resources.displayMetrics.widthPixels * 0.90).toInt()
        setContentView(R.layout.dialog_security_prompt)
        window?.setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT)

        ivOkSecurityPrompt.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivOkSecurityPrompt -> {
                if (checkSecurityCode()) {
                    securityCodeChecked.onCodeChecked()
                    dismiss()
                }
            }
        }
    }

    private fun checkSecurityCode(): Boolean {
        return if (etSecurityCodePrompt.text.toString() == getStringPreference(KEY_SECURITY_CODE, activity)) {
            alarmPlayer.continueAlarm = false
            botchedAttempts = 0
            true
        } else {
            etSecurityCodePrompt.text.clear()
            etSecurityCodePrompt.error = activity.resources.getString(R.string.wrong_code)
            ++botchedAttempts
            if (botchedAttempts == 3) {
                alarmPlayer.startAlarm()
            }
            false
        }
    }

    override fun dismiss() {
        etSecurityCodePrompt?.text?.clear()
        super.dismiss()
    }

}