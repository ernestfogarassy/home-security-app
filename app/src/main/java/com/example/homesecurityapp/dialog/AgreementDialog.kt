package com.example.homesecurityapp.dialog

import android.app.ActionBar
import android.app.Dialog
import android.content.Context
import android.view.View
import com.example.homesecurityapp.R
import kotlinx.android.synthetic.main.dialog_agreement.*


class AgreementDialog(context: Context) : Dialog(context), View.OnClickListener {

    private lateinit var type: AgreementType

    private lateinit var agreementListener: OnAgreementClicked

    interface OnAgreementClicked {
        fun onYesClicked(type: AgreementType)
    }

    init {
        val width = (context.resources.displayMetrics.widthPixels * 0.90).toInt()
        setContentView(R.layout.dialog_agreement)
        window?.setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT)

        tvAgreementCancel.setOnClickListener(this)
        tvAgreementYes.setOnClickListener(this)
    }

    fun show(question: String, type: AgreementType, listener: OnAgreementClicked) {
        this.agreementListener = listener
        this.type = type
        tvAgreementQuestion.text = question
        super.show()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            tvAgreementCancel.id -> {
                dismiss()
            }
            tvAgreementYes.id -> {
                dismiss()
                agreementListener.onYesClicked(type)
            }
        }
    }

    enum class AgreementType {
        LOGOUT
    }
}