package com.example.homesecurityapp.dialog

import android.app.ActionBar
import android.app.Dialog
import android.content.Context
import android.view.View
import com.example.homesecurityapp.R
import kotlinx.android.synthetic.main.dialog_confirmation.*

class ConfirmationDialog(context: Context) : Dialog(context), View.OnClickListener {

    private lateinit var type: ConfirmationType

    private lateinit var confirmationListener: OnConfirmationClicked

    interface OnConfirmationClicked {
        fun onOkClicked(type: ConfirmationType)
    }

    init {
        val width = (context.resources.displayMetrics.widthPixels * 0.90).toInt()
        setContentView(R.layout.dialog_confirmation)
        window?.setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT)

        tvConfirmationOk.setOnClickListener(this)
    }

    fun show(question: String, type: ConfirmationType, listener: OnConfirmationClicked) {
        this.confirmationListener = listener
        this.type = type
        tvConfirmationText.text = question
        super.show()
    }

    enum class ConfirmationType {
        PERMISSION_CAMERA
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            tvConfirmationOk.id -> {
                dismiss()
                confirmationListener.onOkClicked(type)
            }
        }
    }
}