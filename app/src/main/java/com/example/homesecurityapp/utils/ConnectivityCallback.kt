package com.example.homesecurityapp.utils

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities

class ConnectivityCallback(private val connectionChangedListener: ConnectionChangedListener) : ConnectivityManager.NetworkCallback() {

    private var oldState = false

    interface ConnectionChangedListener{

        fun onConnectionChanged(hasNetwork: Boolean)
    }

    override fun onCapabilitiesChanged(network: Network, capabilities: NetworkCapabilities) {
        val connected = capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        if(oldState != connected){
            oldState = connected
            connectionChangedListener.onConnectionChanged(connected)
        }
    }
    override fun onLost(network: Network) {
        if(oldState){
            oldState = false
            connectionChangedListener.onConnectionChanged(false)
        }
    }
}