package com.example.homesecurityapp.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

public class SendMailTask extends AsyncTask {

    private ProgressDialog statusDialog;

    private Activity sendMailActivity;

    private MailSentListener mailSentListener;

    private Boolean displayMessages = true;

    private Boolean sendImage;

    public interface MailSentListener {

        void onMailSentSuccess();

        void onMailSentFailure(String error);
    }

    public void setMailSentListener(MailSentListener mailSentListener) {
        this.mailSentListener = mailSentListener;
    }

    public SendMailTask(Activity activity, Boolean displayMessages, Boolean sendImage) {
        sendMailActivity = activity;
        this.displayMessages = displayMessages;
        this.sendImage = sendImage;
    }

    protected void onPreExecute() {
        statusDialog = new ProgressDialog(sendMailActivity);
        if (displayMessages) {
            statusDialog.setMessage("Getting ready...");
            statusDialog.setIndeterminate(false);
            statusDialog.setCancelable(false);
            statusDialog.show();
        }
    }

    @Override
    protected Object doInBackground(Object... args) {
        try {
            Log.i("SendMailTask", "About to instantiate GMail...");
            if (displayMessages) publishProgress("Processing input....");
            GMail androidEmail = new GMail(sendMailActivity, sendImage, args[0].toString(),
                    args[1].toString(), args[2].toString(), args[3].toString(),
                    args[4].toString());
            if (displayMessages) publishProgress("Preparing mail message....");
            androidEmail.createEmailMessage();
            if (displayMessages) publishProgress("Sending email....");
            androidEmail.sendEmail();
            if (displayMessages) publishProgress("Email Sent.");
            Log.i("SendMailTask", "Mail Sent.");
            mailSentListener.onMailSentSuccess();
        } catch (Exception e) {
            if(e.getMessage() == null){
                publishProgress(e.toString());
                Log.e("SendMailTask", e.toString(), e);
                mailSentListener.onMailSentFailure(e.toString());
            } else {
                publishProgress(e.getMessage());
                Log.e("SendMailTask", e.getMessage(), e);
                mailSentListener.onMailSentFailure(e.getMessage());
            }
        }
        return null;
    }

    @Override
    public void onProgressUpdate(Object... values) {
        if(values[0] != null && displayMessages){
            statusDialog.setMessage(values[0].toString());
        }
    }

    @Override
    public void onPostExecute(Object result) {
        if (displayMessages) statusDialog.dismiss();
    }

}
