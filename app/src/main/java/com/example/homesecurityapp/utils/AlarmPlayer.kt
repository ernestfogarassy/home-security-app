package com.example.homesecurityapp.utils

import android.app.Activity
import android.content.ContentResolver
import android.media.RingtoneManager
import android.net.Uri
import com.example.homesecurityapp.R

class AlarmPlayer(private val activity: Activity) {

    var continueAlarm = true

    fun startAlarm() {
        try {
            val uri = ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + activity.packageName + "/" + R.raw.alarm
            val notification: Uri = Uri.parse(uri)
            val r = RingtoneManager.getRingtone(activity, notification)
            Thread(Runnable {
                while (continueAlarm) {
                    Thread.sleep(350)
                    r.play()
                }
            }).start()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}