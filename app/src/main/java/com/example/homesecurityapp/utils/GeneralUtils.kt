package com.example.homesecurityapp.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.graphics.BitmapFactory
import android.os.Environment
import android.provider.Settings
import android.util.Base64
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*


class GeneralUtils {

    companion object {

        fun getCurrentTimeMillis(): Long {
            val calendar = Calendar.getInstance()
            return calendar.timeInMillis
        }

        fun convertMillisToTime(millis: Long): String {
            return String.format("%02d:%02d:%02d",
                    java.util.concurrent.TimeUnit.MILLISECONDS.toHours(millis),
                    java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(millis) -
                            java.util.concurrent.TimeUnit.HOURS.toMinutes(java.util.concurrent.TimeUnit.MILLISECONDS.toHours(millis)),
                    java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(millis) -
                            java.util.concurrent.TimeUnit.MINUTES.toSeconds(java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(millis)))
        }

        fun bitmapToString(bitmap: Bitmap): String? {
            val byteArray = ByteArrayOutputStream()
            bitmap.compress(CompressFormat.PNG, 100, byteArray)
            bitmap.recycle()
            val b = byteArray.toByteArray()
            return Base64.encodeToString(b, Base64.DEFAULT)
        }

        fun stringToBitmap(encodedString: String): Bitmap? {
            return try {
                val encodeByte = Base64.decode(encodedString, Base64.DEFAULT)
                BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
            } catch (e: Exception) {
                e.message
                null
            }
        }

        fun getScreenWidth(): Int {
            return Resources.getSystem().displayMetrics.widthPixels
        }

        fun saveImage(context: Context, finalBitmap: Bitmap) {
            val root: String = context.getExternalFilesDir(null).toString()
            val myDir = File("$root/homesecurityapp_images")
            myDir.mkdirs()
//            val generator = Random()
//            var n = 10000
//            n = generator.nextInt(n)
            val fname = "image01.jpg"
            val file = File(myDir, fname)
            if (file.exists()) file.delete()
            try {
                val out = FileOutputStream(file)
                finalBitmap.compress(CompressFormat.JPEG, 100, out)
                out.flush()
                out.close()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

        @SuppressLint("HardwareIds")
        fun getDeviceId(context: Context): String? {
            return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
        }
    }
}