package com.example.homesecurityapp.utils

import android.content.Context
import android.view.View
import android.widget.Toast

fun changeViewVisibility(visible: Boolean, vararg views: View?) = views.forEach {
    it?.visibility = if (visible) View.VISIBLE else View.GONE
}

fun toast(message: String, context: Context) = Toast.makeText(context, message, Toast.LENGTH_SHORT).show()