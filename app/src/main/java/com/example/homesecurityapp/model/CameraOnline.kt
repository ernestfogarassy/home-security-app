package com.example.homesecurityapp.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class CameraOnline(

        var start: String = "",

        var end: String = "",

        var facesDetected: Int = 0
)