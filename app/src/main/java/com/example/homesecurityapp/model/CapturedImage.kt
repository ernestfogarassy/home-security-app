package com.example.homesecurityapp.model

data class CapturedImage(

        var start: String = "",

        var image: String = "",

        var duration: String = "",

        var facesDetected: Int = 0
)