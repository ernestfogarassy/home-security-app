package com.example.homesecurityapp.model

data class User (

    var username: String = "",

    var email: String? = ""
)