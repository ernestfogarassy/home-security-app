package com.example.homesecurityapp.model

data class Disturbance(

        var date: String = "",

        var duration: Long = 0,

        var intensity: Int = 0
)