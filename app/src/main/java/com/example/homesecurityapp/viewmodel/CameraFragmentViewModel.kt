package com.example.homesecurityapp.viewmodel

import android.annotation.SuppressLint
import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import com.example.homesecurityapp.Constants.Companion.TIME_FORGET_FACE
import com.example.homesecurityapp.Constants.Companion.TIME_RECOGNIZE_FACE
import com.example.homesecurityapp.utils.GeneralUtils.Companion.bitmapToString
import com.example.homesecurityapp.utils.GeneralUtils.Companion.convertMillisToTime
import com.example.homesecurityapp.utils.GeneralUtils.Companion.getCurrentTimeMillis
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import org.opencv.android.CameraBridgeViewBase
import org.opencv.android.Utils.matToBitmap
import org.opencv.core.Mat
import org.opencv.core.MatOfRect
import org.opencv.core.Point
import org.opencv.core.Scalar
import org.opencv.imgproc.Imgproc
import org.opencv.objdetect.CascadeClassifier
import java.text.SimpleDateFormat
import java.util.*

class CameraFragmentViewModel(private val sessionCounterChange: SessionCounterChange, private val newFaceDetected: NewFaceDetected) :
        ViewModel(), CameraBridgeViewBase.CvCameraViewListener2 {

    private var auth: FirebaseAuth = Firebase.auth

    private lateinit var mRgba: Mat

    private lateinit var mGrey: Mat

    private var startTime: String? = null

    private var isFaceDetected = false

    private var faceDetectedTime: Long? = null

    private var imageUploadedSessionTime: String? = null

    private var faceDetectionStart: Long? = null

    private var multipleFacesDetectedTime: Long? = null

    private var sessionFaceCounter: Int = 0
        set(value) {
            field = value
            sessionCounterChange.onCounterChanged(value)
        }

    private var facesPerImage = 0

    var faceDetector: CascadeClassifier? = null

    var isCaptureRunning = false

    interface SessionCounterChange {

        fun onCounterChanged(counter: Int)
    }

    interface NewFaceDetected {

        fun onFaceDetected(bitmapString: Bitmap?, time: String)
    }

    @SuppressLint("SimpleDateFormat")
    fun startCapture() {
        isCaptureRunning = true
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm")
        startTime = sdf.format(Date())

        FirebaseDatabase.getInstance().reference.child("users")
                .child(auth.uid!!).child("cameraOnline")
                .child(startTime!!).child("start").setValue(startTime)
    }

    @SuppressLint("SimpleDateFormat")
    fun stopCapture() {
        if (isFaceDetected) {
            val myRef = FirebaseDatabase.getInstance().reference

            val currentTime = getCurrentTimeMillis()
            val startFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val formattedStartTime = startFormat.format(faceDetectionStart)

            myRef.child("users").child(auth.uid!!).child("capturedImage")
                    .child(formattedStartTime).child("duration")
                    .setValue(convertMillisToTime(currentTime - faceDetectionStart!!))
        }
        deleteCaptureData()
        isCaptureRunning = false
        sessionFaceCounter = 0
        facesPerImage = 0
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm")
        val currentDate = sdf.format(Date())

        val myRef = FirebaseDatabase.getInstance().reference
        myRef.child("users").child(auth.uid!!).child("cameraOnline")
                .child(startTime!!).child("end").setValue(currentDate)
    }

    @SuppressLint("SimpleDateFormat")
    private fun onFaceDetected(numberOfFaces: Int, mat: Mat) {
        val currentTime = getCurrentTimeMillis()
        if (isFaceDetected) {
            // check if more faces have been detected
            if (numberOfFaces > facesPerImage) {
                when {
                    multipleFacesDetectedTime == null -> {
                        multipleFacesDetectedTime = currentTime
                    }
                    currentTime - multipleFacesDetectedTime!! > TIME_FORGET_FACE -> {
                        multipleFacesDetectedTime = currentTime
                    }
                    currentTime - multipleFacesDetectedTime!! > TIME_RECOGNIZE_FACE -> {
                        sessionFaceCounter += numberOfFaces - facesPerImage
                        facesPerImage = numberOfFaces
                        val myRef = FirebaseDatabase.getInstance().reference
                        myRef.child("users").child(auth.uid!!).child("cameraOnline")
                                .child(startTime!!).child("facesDetected")
                                .setValue(sessionFaceCounter)

                        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                        val formattedTime = sdf.format(faceDetectionStart)
                        myRef.child("users").child(auth.uid!!).child("capturedImage")
                                .child(formattedTime).child("facesDetected").setValue(sessionFaceCounter)

                        uploadImage(mat, myRef, imageUploadedSessionTime!!)
                    }
                }
            }
        }
        if (faceDetectedTime == null) {
            faceDetectedTime = currentTime
            faceDetectionStart = currentTime
        } else if (!isFaceDetected && faceDetectedTime != null) {
            if (currentTime - faceDetectedTime!! > TIME_RECOGNIZE_FACE) {
                isFaceDetected = true
                faceDetectedTime = currentTime
                sessionFaceCounter += numberOfFaces
                facesPerImage = numberOfFaces

                val myRef = FirebaseDatabase.getInstance().reference
                myRef.child("users").child(auth.uid!!).child("cameraOnline")
                        .child(startTime!!).child("facesDetected").setValue(sessionFaceCounter)

                val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                imageUploadedSessionTime = sdf.format(faceDetectionStart)
                myRef.child("users").child(auth.uid!!).child("capturedImage")
                        .child(imageUploadedSessionTime!!).child("start").setValue(imageUploadedSessionTime)
                myRef.child("users").child(auth.uid!!).child("capturedImage")
                        .child(imageUploadedSessionTime!!).child("facesDetected").setValue(sessionFaceCounter)

                uploadImage(mat, myRef, imageUploadedSessionTime!!)
            }
        }
    }

    override fun onCameraViewStarted(width: Int, height: Int) {
        mRgba = Mat()
        mGrey = Mat()
    }

    override fun onCameraViewStopped() {
        mRgba.release()
        mGrey.release()
    }

    private fun checkDuration(numberOfFaces: Int): Boolean {
        if (isFaceDetected && numberOfFaces <= facesPerImage) {
            val calendar = Calendar.getInstance()
            val currentTime = calendar.timeInMillis
            // if the face has been detected within a time frame, ignore it
            if (currentTime - faceDetectedTime!! < TIME_FORGET_FACE) {
                faceDetectedTime = currentTime
                return false
            }
        }
        return true
    }

    @SuppressLint("SimpleDateFormat")
    private fun checkFaceDuration() {
        val currentTime = getCurrentTimeMillis()
        if (currentTime - faceDetectedTime!! >= TIME_FORGET_FACE) {
            uploadDuration(currentTime)
            deleteCaptureData()
        }
    }

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame): Mat {
        mRgba = inputFrame.rgba()
        mGrey = inputFrame.gray()

        // detect face
        val faceDetections = MatOfRect()
        faceDetector!!.detectMultiScale(mRgba, faceDetections)
        if (isFaceDetected && faceDetections.toArray().isEmpty()) {
            checkFaceDuration()
            return mRgba
        }
        for (rect in faceDetections.toArray()) {
            Imgproc.rectangle(mRgba, Point(rect.x.toDouble(), rect.y.toDouble()),
                    Point((rect.x + rect.width).toDouble(), (rect.y + rect.height).toDouble()),
                    Scalar(46.0, 67.0, 114.0))
            if (isCaptureRunning) {
                if (checkDuration(faceDetections.toArray().size)) {
                    onFaceDetected(faceDetections.toArray().size, mRgba)
                }
            }
        }
        return mRgba
    }

    @SuppressLint("SimpleDateFormat")
    private fun uploadDuration(currentTime: Long) {
        val myRef = FirebaseDatabase.getInstance().reference

        val startFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val formattedStartTime = startFormat.format(faceDetectionStart)

        myRef.child("users").child(auth.uid!!).child("capturedImage")
                .child(formattedStartTime).child("duration")
                .setValue(convertMillisToTime(currentTime - faceDetectionStart!! - TIME_FORGET_FACE))
    }

    private fun uploadImage(mat: Mat, myRef: DatabaseReference, formattedTime: String) {
        val newMat = Mat()
        Imgproc.cvtColor(mat, newMat, Imgproc.COLOR_BGR2RGB)
        val resultBitmap = Bitmap.createBitmap(newMat.cols(), newMat.rows(), Bitmap.Config.ARGB_8888)
        matToBitmap(mat, resultBitmap)
        if (newMat.isContinuous && resultBitmap != null) {
            newFaceDetected.onFaceDetected(resultBitmap, formattedTime)
            val bitmapByte = bitmapToString(resultBitmap)
            myRef.child("users").child(auth.uid!!).child("capturedImage")
                    .child(formattedTime).child("image").setValue(bitmapByte)
        }
    }


    private fun deleteCaptureData() {
        isFaceDetected = false
        faceDetectedTime = null
        faceDetectionStart = null
        multipleFacesDetectedTime = null
    }

}