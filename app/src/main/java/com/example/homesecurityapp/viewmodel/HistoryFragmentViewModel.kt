package com.example.homesecurityapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.homesecurityapp.model.CapturedImage
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase

class HistoryFragmentViewModel : ViewModel() {

    private var auth = Firebase.auth

    var historyListLiveData = MutableLiveData<ArrayList<CapturedImage>>()

    fun queryCapturedImage() {
        val myRef = FirebaseDatabase.getInstance().reference
        val query = myRef.child("users").child(auth.uid!!).child("capturedImage").orderByValue()
        query.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                //no need to implement
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val dataObjectList = arrayListOf<CapturedImage>()
                for (dataObject in dataSnapshot.children) {
                    val capturedImage = dataObject.getValue(CapturedImage::class.java)
                    if (capturedImage != null) {
                        dataObjectList.add(capturedImage)
                    }
                }
                historyListLiveData.value = dataObjectList
            }
        })
    }
}