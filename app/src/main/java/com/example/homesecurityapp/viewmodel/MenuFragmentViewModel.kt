package com.example.homesecurityapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.homesecurityapp.model.CameraOnline
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase

class MenuFragmentViewModel : ViewModel() {

    private var auth = Firebase.auth

    var cameraOnlineMutableList = MutableLiveData<ArrayList<CameraOnline>>()

    var usernameLiveData = MutableLiveData<String>()

    fun queryUsername() {
        val myRef = FirebaseDatabase.getInstance().reference
        val query = myRef.child("users").child(auth.uid!!).child("username").orderByValue()
        query.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                //no need to implement
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                usernameLiveData.value = dataSnapshot.value.toString()
            }
        })
    }

    fun queryCameraOnline() {
        val myRef = FirebaseDatabase.getInstance().reference
        val query = myRef.child("users").child(auth.uid!!).child("cameraOnline").orderByValue()
        query.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                //no need to implement
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val dataObjectList = arrayListOf<CameraOnline>()
                for (dataObject in dataSnapshot.children) {
                    val cameraOnline = dataObject.getValue(CameraOnline::class.java)
                    if (cameraOnline != null) {
                        dataObjectList.add(cameraOnline)
                    }
                }
                cameraOnlineMutableList.value = dataObjectList
            }
        })
    }
}