package com.example.homesecurityapp.handler

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.homesecurityapp.dialog.AgreementDialog
import com.example.homesecurityapp.R
import com.example.homesecurityapp.view.*

class AuthFragmentHandler(private val baseActivity: BaseActivity) : AgreementDialog.OnAgreementClicked {

    private val greetingFragment = GreetingFragment(baseActivity)

    private val registerFragment = RegisterFragment(baseActivity)

    private val loginFragment = LoginFragment(baseActivity)

    private val verificationFragment = VerificationFragment(baseActivity)

    private val agreementDialog = AgreementDialog(baseActivity)

    private var isGreetingShown = false

    private var isRegisterShown = false

    private var isLoginShown = false

    private var isVerificationShown = false

    fun showFragment(fragment: AuthFragments) {
        val fm: FragmentManager = baseActivity.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fm.beginTransaction()

        isGreetingShown = false
        isRegisterShown = false
        isLoginShown = false
        isVerificationShown = false

        when (fragment) {
            AuthFragments.GREETING -> {
                fragmentTransaction.replace(R.id.containerLayout, greetingFragment)
                isGreetingShown = true
            }
            AuthFragments.REGISTER -> {
                fragmentTransaction.replace(R.id.containerLayout, registerFragment)
                isRegisterShown = true
            }
            AuthFragments.LOGIN -> {
                fragmentTransaction.replace(R.id.containerLayout, loginFragment)
                isLoginShown = true
            }
            AuthFragments.VERIFICATION -> {
                fragmentTransaction.replace(R.id.containerLayout, verificationFragment)
                isVerificationShown = true
            }
        }
        fragmentTransaction.commit()
    }

    fun getShownFragment(): AuthFragments {
        return when {
            isRegisterShown -> AuthFragments.REGISTER
            isLoginShown -> AuthFragments.LOGIN
            isVerificationShown -> AuthFragments.VERIFICATION
            else -> AuthFragments.GREETING
        }
    }

    fun clearAuthFields(){
        loginFragment.clearFields()
        registerFragment.clearFields()
        verificationFragment.clearFields()
    }

    fun onBackPressed() {
        when {
            isGreetingShown -> agreementDialog.show(baseActivity.resources.getString(R.string.agree_exit),
                    AgreementDialog.AgreementType.LOGOUT, this)
            isRegisterShown -> showFragment(AuthFragments.GREETING)
            isLoginShown -> showFragment(AuthFragments.GREETING)
            isVerificationShown -> showFragment(AuthFragments.REGISTER)
        }
    }

    override fun onYesClicked(type: AgreementDialog.AgreementType) {
        when(type){
            AgreementDialog.AgreementType.LOGOUT -> {
                baseActivity.exitApp()
            }
        }
    }
}

enum class AuthFragments {
    GREETING,
    REGISTER,
    LOGIN,
    VERIFICATION
}