package com.example.homesecurityapp.handler

import android.app.Activity
import android.content.Context

class PreferenceHandler {

    companion object {

        fun containsPreference(key: String, activity: Activity): Boolean{
            val sharedPref = activity.getPreferences(Context.MODE_PRIVATE) ?: return false
            return sharedPref.contains(key)
        }

        fun removePreference(key: String, activity: Activity){
            val sharedPref = activity.getPreferences(Context.MODE_PRIVATE) ?: return
            with(sharedPref.edit()){
                remove(key)
                commit()
            }
        }

        fun deleteAllPreferences(activity: Activity){
            val sharedPref = activity.getPreferences(Context.MODE_PRIVATE) ?: return
            with(sharedPref.edit()){
                clear()
                commit()
            }
        }

        fun saveStringPreference(key: String, value: String, activity: Activity) {
            val sharedPref = activity.getPreferences(Context.MODE_PRIVATE) ?: return
            with(sharedPref.edit()) {
                putString(key, value)
                commit()
            }
        }

        fun getStringPreference(key: String, activity: Activity): String? {
            val sharedPref = activity.getPreferences(Context.MODE_PRIVATE) ?: return null
            return sharedPref.getString(key, null)
        }
    }
}