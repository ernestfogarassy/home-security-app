package com.example.homesecurityapp.handler

import android.content.Context
import android.view.OrientationEventListener

class OrientationHandler(context: Context, private val listener: OrientationChangedListener) : OrientationEventListener(context) {

    var currentOrientation: DevicePosition? = null

    init {
        enable()
    }

    interface OrientationChangedListener {
        fun onOrientationChanged(position: DevicePosition)
    }

    override fun onOrientationChanged(orientation: Int) {
        when (orientation) {
            in MIN_ORIENTATION_DEGREES..MAX_ORIENTATION_DEGREES -> {
                if (currentOrientation != DevicePosition.CORRECT_POSITION) {
                    currentOrientation = DevicePosition.CORRECT_POSITION
                    listener.onOrientationChanged(DevicePosition.CORRECT_POSITION)
                }
            }
            in MIN_ORIENTATION_FLIP..MAX_ORIENTATION_FLIP -> {
                if (currentOrientation != DevicePosition.TO_FLIP) {
                    currentOrientation = DevicePosition.TO_FLIP
                    listener.onOrientationChanged(DevicePosition.TO_FLIP)
                }
            }
            else -> {
                if (currentOrientation != DevicePosition.TO_ROTATE) {
                    currentOrientation = DevicePosition.TO_ROTATE
                    listener.onOrientationChanged(DevicePosition.TO_ROTATE)
                }
            }
        }
    }

    companion object {

        const val MAX_ORIENTATION_DEGREES = 280

        const val MIN_ORIENTATION_DEGREES = 260

        const val MIN_ORIENTATION_FLIP = 70

        const val MAX_ORIENTATION_FLIP = 110
    }
}

enum class DevicePosition {
    CORRECT_POSITION,
    TO_ROTATE,
    TO_FLIP
}