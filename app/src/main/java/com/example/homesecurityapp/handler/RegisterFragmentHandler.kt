package com.example.homesecurityapp.handler

import android.annotation.SuppressLint
import com.example.homesecurityapp.Constants
import com.example.homesecurityapp.utils.SendMailTask
import com.example.homesecurityapp.utils.toast
import com.example.homesecurityapp.view.BaseActivity
import java.util.*
import kotlin.random.Random

class RegisterFragmentHandler(private val baseActivity: BaseActivity) : SendMailTask.MailSentListener {

    fun sendEmail() {
        val sendMailTask = SendMailTask(baseActivity, true, false)
        sendMailTask.setMailSentListener(this)
        sendMailTask.execute(Constants.SENDER_EMAIL, Constants.SENDER_PASS, PreferenceHandler.getStringPreference(Constants.KEY_EMAIL, baseActivity),
                "Verification code", "Copy this code to verify your email: <center><BR><b>" +
                PreferenceHandler.getStringPreference(Constants.KEY_VERIFICATION_CODE, baseActivity) + "</b><BR><BR></center>" +
                "<BR>This is a verification code generated for the Home Security Application." +
                "<BR>If you haven't requested a code, please ignore this message.")
    }

    @SuppressLint("SimpleDateFormat")
    fun saveUserData(username: String, email: String, password: String) {
        val calendar = Calendar.getInstance()
        val time = calendar.timeInMillis / 60000
        PreferenceHandler.saveStringPreference(Constants.KEY_TEMP_USER, username, baseActivity)
        PreferenceHandler.saveStringPreference(Constants.KEY_EMAIL, email, baseActivity)
        PreferenceHandler.saveStringPreference(Constants.KEY_PASS, password, baseActivity)
        PreferenceHandler.saveStringPreference(Constants.KEY_CODE_TIME, time.toString(), baseActivity)
        PreferenceHandler.saveStringPreference(Constants.KEY_VERIFICATION_CODE, generateCode(), baseActivity)
    }

    @SuppressLint("SimpleDateFormat")
    private fun generateCode(): String {
        val calendar = Calendar.getInstance()
        val time = calendar.timeInMillis / 60000
        val dateCode = (Random.nextInt(9) + 1).toString() + time.toString()
        return Integer.toHexString(dateCode.toInt()).toUpperCase(Locale.getDefault())
    }

    override fun onMailSentSuccess() {
        baseActivity.navigateToAuthFragment(AuthFragments.VERIFICATION)
    }

    override fun onMailSentFailure(error: String?) {
        error?.let {
            baseActivity.runOnUiThread {
                toast(it, baseActivity)
            }
        }
        PreferenceHandler.deleteAllPreferences(baseActivity)
    }
}