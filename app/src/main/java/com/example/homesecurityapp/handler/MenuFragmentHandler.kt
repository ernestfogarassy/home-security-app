package com.example.homesecurityapp.handler

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.homesecurityapp.R
import com.example.homesecurityapp.dialog.AgreementDialog
import com.example.homesecurityapp.view.BaseActivity
import com.example.homesecurityapp.view.CameraFragment
import com.example.homesecurityapp.view.HistoryFragment
import com.example.homesecurityapp.view.MenuFragment


class MainFragmentHandler(private val baseActivity: BaseActivity) : AgreementDialog.OnAgreementClicked, CameraFragment.CodeInputCorrectly {

    private val menuFragment = MenuFragment(baseActivity)

    private val cameraFragment = CameraFragment(baseActivity, this)

    private val historyFragment = HistoryFragment(baseActivity)

    private val agreementDialog = AgreementDialog(baseActivity)

    var fragmentToShow: MainFragments? = null

    private var isCameraShown = false

    private var isMenuShown = false

    private var isHistoryShown = false

    fun showFragment(fragment: MainFragments) {
        if (fragment == MainFragments.CAMERA && !baseActivity.checkCameraPermission()) {
            return
        }
        if (isCameraShown && cameraFragment.cameraFragmentViewModel.isCaptureRunning) {
            fragmentToShow = fragment
            cameraFragment.stopCapture()
            return
        }
        val fm: FragmentManager = baseActivity.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fm.beginTransaction()

        isCameraShown = false
        isMenuShown = false
        isHistoryShown = false

        when (fragment) {
            MainFragments.CAMERA -> {
                fragmentTransaction.replace(R.id.containerLayout, cameraFragment)
                isCameraShown = true
            }
            MainFragments.MENU -> {
                fragmentTransaction.replace(R.id.containerLayout, menuFragment)
                isMenuShown = true
            }
            MainFragments.HISTORY -> {
                fragmentTransaction.replace(R.id.containerLayout, historyFragment)
                isHistoryShown = true
            }
        }
        fragmentTransaction.commit()
    }

    override fun onCodeInput() {
        fragmentToShow?.let {
            showFragment(it)
            fragmentToShow = null
        }
    }

    fun getShownFragment(): MainFragments {
        return when {
            isCameraShown -> MainFragments.CAMERA
            isHistoryShown -> MainFragments.HISTORY
            else -> MainFragments.MENU
        }
    }

    fun onBackPressed() {
        when {
            isMenuShown -> agreementDialog.show(baseActivity.resources.getString(R.string.agree_exit),
                    AgreementDialog.AgreementType.LOGOUT, this)
            isCameraShown -> showFragment(MainFragments.MENU)
            isHistoryShown -> showFragment(MainFragments.MENU)
        }
    }

    override fun onYesClicked(type: AgreementDialog.AgreementType) {
        when (type) {
            AgreementDialog.AgreementType.LOGOUT -> {
                baseActivity.exitApp()
            }
        }
    }
}

enum class MainFragments {
    MENU,
    CAMERA,
    HISTORY
}