package com.example.homesecurityapp

class Constants {
    companion object {

        const val SENDER_EMAIL = "homeappmail2020@gmail.com"

        const val SENDER_PASS = "androIIIdandroIIId"

        const val MIN_USERNAME_LENGTH = 6

        const val MIN_PASSWORD_LENGTH = 6

        const val MIN_VALIDATION_LENGTH = 4

        const val MIN_SECURITY_CODE_LENGTH = 4

        const val CAMERA_PERMISSION_REQUEST_CODE = 200

        const val CODE_VALIDITY = 30

        const val TIME_FORGET_FACE = 2000

        const val TIME_RECOGNIZE_FACE = 1000

        const val KEY_CODE_TIME = "KEY_CODE_TIME"

        const val KEY_VERIFICATION_CODE = "KEY_VERIFICATION_CODE"

        const val KEY_SECURITY_CODE = "KEY_SECURITY_CODE"

        const val KEY_EMAIL = "KEY_EMAIL"

        const val KEY_PASS = "KEY_PASS"

        const val KEY_TEMP_USER = "KEY_TEMP_USER"
    }
}